<?php

use App\Http\Controllers\RssController;
use App\Http\Controllers\ProjectController;
use Illuminate\Support\Facades\Route;

/*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

Route::view('/', 'welcome')->name('home');
Route::view('/demo', 'demo');
Route::view('/projects-list', 'projects-list');
Route::view('/about', 'about')->name('about');
Route::get('/rss', [RssController::class, 'feed'])->name('feed');
Route::get('/rss/{slug}', [RssController::class, 'feedTag'])->name('feed-tag');
Route::resource('projects', ProjectController::class)->except(['edit', 'update', 'destroy']);
Route::get('/projects/{project}/success', [ProjectController::class, 'success'])->name('projects.success');
Route::get('/projects/{project}/edit/success', [ProjectController::class, 'edit_success'])->name('projects.edit.success');
Route::group(['middleware' => ['can:manage,project,token']], function () {
    Route::get('/projects/{project}/edit/{token}', [ProjectController::class, 'edit'])->name('projects.edit');
    Route::patch('/projects/{project}/{token}', [ProjectController::class, 'update'])->name('projects.update');
    Route::get('/projects/{project}/{token}', [ProjectController::class, 'destroy'])->name('projects.destroy');
});
Route::view('/tips-contribution', 'tips-contribution')->name('tips-contribution');
