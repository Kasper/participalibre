<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CreateSuccessfully extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('[' . config('app.name') . '] Publication de votre annonce')
            ->greeting('Votre annonce a été publiée sur Contribulle')
            ->line('Merci d’avoir créé une annonce sur Contribulle.')
            ->line('Vous pouvez la modifier ou la supprimer à l’aide de ce lien: ')
            ->action("Modifier ou supprimer mon annonce", $notifiable->path_admin)
            ->line('Si vous n\'arrivez pas à cliquer sur le bouton, copiez-collez ce lien dans votre navigateur 😉 : '. $notifiable->path_admin)
            ->line('N’hésitez pas à diffuser votre annonce autant que vous le souhaitez !')
            ->line('Pour faire un retour sur les contributions effectuées, envoyez-nous un mail. Vos témoignages, ainsi que ceux des personnes qui ont contribué à votre projet, nous sont importants pour pouvoir améliorer Contribulle selon vos besoins.')
            ->line('Bullement vôtre 🙂')
            ->salutation('Le collectif Contribulle');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
