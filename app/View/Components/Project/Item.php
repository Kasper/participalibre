<?php

namespace App\View\Components\Project;

use Illuminate\View\Component;

class Item extends Component
{


    public $project;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($project)
    {
        $this->project = $project;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.project.item');
    }
}
