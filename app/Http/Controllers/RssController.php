<?php

namespace App\Http\Controllers;

use App\Models\Project;

class RssController extends Controller
{
    public function feed()
    {
        return response()->view('feed', ['projects' => Project::all()])->header('Content-Type', 'application/xml');
    }

    public function feedTag($slug)
    {
        $project = Project::whereHas('tags', function ($q) use ($slug) {
            $q->where('slug', '=', $slug);
        })->get();
        return response()->view('feed', ['projects' => $project])->header('Content-Type', 'application/xml');
    }
}
