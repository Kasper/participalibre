const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/accordion.js', 'public/js')
    .js('resources/js/slider.js', 'public/js')
    .js('resources/js/easymde.js', 'public/js')
    .postCss('resources/css/easymde.css', 'public/css')
    .js('resources/js/filter-menu.js', 'public/js')
    .js('resources/js/filters-ajax.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
