class Accordions {

  /**
   * Represents an accordion elements group.
   * @params {string} className - The className of an accordion.
   */
  constructor(className) {
    this.accordions = [];

    const parentElement = document.querySelector("." + className).parentElement 

    this.hideAll = parentElement.getAttribute('accordions-hide-all') !== null ? true : false 

    document.querySelectorAll("." + className).forEach((el) => {
      const toggleElement = el.querySelector(".toggle-element");
      toggleElement.addEventListener("click", (ev) => this.clicked(ev.target));

      this.accordions.push({
        el: el,
        toggleElement,
        button: el.querySelector(".chevron"),
        expand: !!toggleElement.className.search("chevron-up"),
      });
    });
    this.computeHeights();
    this.hideAllContent();


    window.addEventListener('resize', this.computeHeights.bind(this))
  }

  computeHeights() {
    this.accordions.forEach((element) => {
      element.el.style.height = null;
      element.elInitHeight = element.el.clientHeight;
      element.contentInitHeight = element.el.querySelector(".content").clientHeight;
      if (element.expand) {
        this.displayContent(element);
      } else {
        this.hideContent(element);
      }
    })
  }

  /**
   * 
   * @param {HTMLElement} button element clicked
   */
  clicked(button) {
    const el = this.accordions.find(
      (accordion) =>
        accordion.toggleElement === button ||
        accordion.toggleElement === button.parentElement ||
        accordion.toggleElement === button.parentElement.parentElement
    );
    if (el.expand) {
      this.hideContent(el);
    } else {
      this.displayContent(el);
    }
  }

  /**
   * 
   * @param {Object} el
   */
  hideContent(el) {
    // el.content.className += ' hidden';
    el.el.style.height = el.elInitHeight - el.contentInitHeight + "px";
    el.toggleElement.className = el.toggleElement.className.replace('opened', '')
    el.button.className = el.button.className.replace(
      "chevron-up",
      "chevron-down"
    );
    el.expand = false;
  }

  /**
   * 
   * @param {Object} el 
   */
  displayContent(el) {
    if (this.hideAll) {
      this.hideAllContent();      
    }

    el.expand = true;
    el.el.style.height = el.elInitHeight + "px";
    if (el.toggleElement.className.search('opened') < 0) {
      el.toggleElement.className += ' opened'      
    }
    el.button.className = el.button.className.replace(
      "chevron-down",
      "chevron-up"
    );
  }

  /**
   * Calls the hideContent method for all elements.
   */
  hideAllContent() {
    this.accordions.forEach((accordion) => {
      this.hideContent(accordion);
    });
  }
}

new Accordions("accordion");