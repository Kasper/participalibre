class FilterMenu {
    /**
     *
     * @param {String} selector The selector on the menu element
     * @param {String} toggleSelector Selector of the toggleElement
     * @param {Object} options Options for the filter menu
     * @param {String} [options.top="10%"] Percentage of space on the top, when the menu is open.
     * @param {String} [options.default="hide"] Default status of the menu, hide or else.
     * @param {String} [options.breakpoint=1200] Breakpoint where the menu switches to mobile version.
     */
    constructor(
        selector,
        toggleSelector,
        options = { top: "0%", default: "hide", breakpoint: 960 }
    ) {
        this.element = document.querySelector(selector);
        this.toggleElement = document.querySelector(toggleSelector);

        this.options = options;
        this.elements = Array.from(this.element.querySelectorAll("*"));
        this.computeSize();

        if (options.default === "hide") {
            this.hideMenu();
        } else {
            this.displayMenu();
        }

        this.toggleElement.addEventListener(
            "click",
            this.toggleMenu.bind(this)
        );

        window.addEventListener("resize", this.computeSize.bind(this));
    }

    /**
     * Method for compute and determine witch version should be display.
     */
    computeSize() {
        this.isMobile = document.body.clientWidth < this.options.breakpoint;
        if (this.element.opened) {
            this.displayMenu();
        } else {
            this.hideMenu();
        }
    }

    /**
     * Method call for toggle menu.
     */
    toggleMenu() {
        if (this.element.className.search("opened") >= 0) {
            this.hideMenu();
        } else {
            this.displayMenu();
        }
    }

    /**
     * Method for displaying a menu.
     */
    displayMenu() {
        this.element.opened = true;
        this.toggleElement.className = this.toggleElement.className.split(
            "inverse"
        )[0];

        this.element.style.top = this.top;
        this.toggleElement.innerHTML = "<i class='filter-close'></i> Fermer";
        if (this.element.className.search("opened") < 0) {
            this.element.className += " opened";
        }
    }

    /**
     * Method for hiding a menu.
     */
    hideMenu() {
        this.element.opened = false;
        this.toggleElement.className += " inverse";

        this.element.style.top = this.hideTop;
        this.element.className = this.element.className.replace("opened", "");
        this.toggleElement.innerHTML = "Filtrer les contributions";
    }

    /**
     * Getter for return null if the width of display is not
     * to small.
     */
    get top() {
        return this.isMobile ? this.options.top : null;
    }

    /**
     * Getter for return null if the width of display is not
     * to small.
     */
    get hideTop() {
        return this.isMobile ? "100%" : null;
    }
}

new FilterMenu("#filters-form", "#filter-toggle-button");
