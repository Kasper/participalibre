class FiltersAjax {
    /**
     * When a checkbox was select the projects list was update
     * and a new tag element appear in the tagsElement.
     * @param {HTMLElement} checkboxElements
     * @param {Array} tagsElement
     * @param {HTMLElement} loaderElement
     * @param {Object} [options.updateOnSelect=true]
     */
    constructor(
        checkboxElements,
        tagsElement,
        loaderElement,
        projectsListElement,
        options = { updateOnSelect: true }
    ) {
        document.querySelector('#filters-button').remove()
        this.tagsElement = tagsElement;
        this.options = options;
        this.tagsSelected = [];
        this.loaderElement = loaderElement;
        this.projectsListElement = projectsListElement;

        this.loaderElement.style.display = "None";

        const url = window.location.href;

        checkboxElements.forEach(checkbox => {
            if (checkbox.checked) {
                this.addTag(checkbox);
            } else if (url.search(`${checkbox.name}=${checkbox.value}`) > -1) {
                this.addTag(checkbox);
            }

            checkbox.addEventListener(
                "click",
                this.updateTagsSelected.bind(this)
            );
        });
    }

    /**
     * Method for deleting a tag.
     * @param {HTMLElement} tagInput
     */
    deleteTag(tagInput) {
        tagInput.checked = false;

        this.tagsSelected = this.tagsSelected.filter(t => t.id !== tagInput.id);
        this.tagsElement.forEach(element =>
            element.removeChild(
                element.querySelector(`#tag_element_${tagInput.id}`)
            )
        );
        this.updateProjects();
    }

    /**
     * Method for add a tag.
     * @param {HTMLElement} tagInput checkbox checked
     */
    addTag(tagInput) {
        tagInput.checked = true;
        this.tagsSelected.push(tagInput);
        this.tagsElement.forEach(element =>
            element.appendChild(this.createTag(tagInput))
        );
        this.updateProjects();
    }

    /**
     * Method for set list of project.
     * @param {XMLHttpRequest} ajax request send
     * @param {String} search get parameters send
     */
    setProjects(ajax, search) {
        if (ajax.readyState === XMLHttpRequest.DONE) {
            if (ajax.status === 200) {
                this.projectsListElement.innerHTML = ajax.responseText;
                this.switchLoader();
                window.history.pushState(
                    "projects",
                    "Les projets qui ont besoin d’aide",
                    "/projects?" + search
                );
            }
        }
    }

    /**
     * Method to make a call to the server to modify the list of projects.
     */
    updateProjects() {
        this.switchLoader();
        try {
            let ajax = new XMLHttpRequest();
            let searchTags = "";

            if (!ajax) {
                throw new Error("Fatal error : XMLHTTP problem.");
            }
            this.tagsSelected.forEach(element => {
                searchTags += `${element.name}=${element.value}&`;
            });

            ajax.onreadystatechange = () => this.setProjects(ajax, searchTags);
            ajax.open("GET", "/api/projects?" + searchTags, true);
            ajax.send();
        } catch (error) {
            console.log(error);
        }
    }

    /**
     * Method for update the list of tags.
     * @param {Event} e event send when the user check an input
     */
    updateTagsSelected(e) {
        if (e.target.checked) {
            this.addTag(e.target);
        } else {
            this.deleteTag(e.target);
        }
    }

    /**
     * Method for create a tagElement.
     * @param {HTMLElement} tag input that has been modified.
     * @returns HTMLElement
     */
    createTag(tag) {
        const tagElement = document.createElement("div");
        const spanElement = document.createElement("span");
        const iElement = document.createElement("i");
        tagElement.className = "filter-tag";
        tagElement.id = `tag_element_${tag.id}`;
        spanElement.innerText = tag.nextElementSibling.innerText;
        iElement.className = "close";
        tagElement.appendChild(spanElement);
        tagElement.appendChild(iElement);

        iElement.addEventListener("click", ev => this.deleteTag(tag));

        return tagElement;
    }

    /**
     * Method for change the displaying of the loader
     * and of the project list.
     */
    switchLoader() {
        if (this.loaderElement.style.display === "block") {
            this.loaderElement.style.display = "None";
            this.projectsListElement.style.display = "block";
        } else {
            this.loaderElement.style.display = "block";
            this.projectsListElement.style.display = "None";
        }
    }
}

new FiltersAjax(
    document.querySelectorAll(".buttons > .checkbox"),
    document.querySelectorAll(".filters-tags"),
    document.querySelector(".filter-loader"),
    document.querySelector("#index")
);
