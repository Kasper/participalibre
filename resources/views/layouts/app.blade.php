<!DOCTYPE html>
<html lang="fr" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title') ⋅ Contribulle</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="shortcut icon" href="/images/logo.png" type="image/x-icon">
    @yield('style')
</head>

<body>
    <header class="big-header">
        <div class="columns flex-space-between">
            <a href="{{ route('home') }}" class="columns flex-items-center logo">

                <img class="margin-small-right" src="/images/logo.svg" alt="Logo de contribulle" height="75px"
                    width="75px">
                <div class="home-link">
                    <h1>
                        Contribulle
                    </h1>
                    <sub>
                        La contribution pour tout le monde
                    </sub>
                </div>
            </a>

            <div class="column text-right menu">
                @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                    <a href="{{ route('home') }}">Home</a>
                    @else
                    <a href="{{ route('login') }}">Login</a>
                    @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                    @endif
                    @endauth
                </div>
                @endif
                <i class="burger-menu"></i>
                <nav class="links text-normal">
                    <x-navbar.link :to="route('home')">
                        Accueil
                    </x-navbar.link>
                    <x-navbar.link :to="route('about')">
                        À propos
                    </x-navbar.link>
                </nav>
            </div>
        </div>
    </header>
    <main>
        <header>
            @yield('content-header')
        </header>
        {{-- 
            Permet de rendre le display: flex uniquement si le content-nav n’est pas vide.
            Voir : https://laravel.com/docs/8.x/blade#section-directives 
        --}}
        @hasSection ('content-nav')
        <section id="main-section">
            <nav id="content-nav">
                @yield('content-nav')
            </nav>
            <section id="content">
                @yield('content')
            </section>
        </section>
        {{-- Sinon, on ne met pas le display: flex ! --}}
        @else
        <section id="content">
            @yield('content')
        </section>
        @endif
    </main>
    <footer class="big-footer">
        <div class="links-footer">
            Pour nous suivre ou nous écrire :
            <div>
                <a target="_blank" href="https://mastodon.tedomum.net/@contribulle"
                    class="fontawesome fontawesome-mastodon"></a>
                <a target="_blank" href="https://twitter.com/contribulle" class="fontawesome fontawesome-twitter"></a>
                <a href="mailto:bonjour[arobase]contribulle.org" class="fontawesome fontawesome-at"></a>
                <a target="_blank" href="https://contribulle.org/rss" class="fontawesome fontawesome-rss"></a>
            </div>
        </div>
        <div>
            Contribulle 2021 — Ce site est placé sous licence
            <a href="https://www.gnu.org/licenses/agpl-3.0.txt" target="_blank">GNU AGPLv3+</a>
            — <a href="https://framagit.org/participalibre/participalibre" target="_blank">Code source</a>
        </div>
    </footer>
    <script defer="true" src="/js/app.js"></script>
    @yield('script')
</body>

</html>