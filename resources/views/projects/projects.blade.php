
            <h1>Les projets qui ont besoin d’un coup de main</h1>
            <div class="child-margin-vertical-medium">
                @foreach ($projects as $project)

                   <x-project.item :project="$project" />
                @endforeach
            </div>
    </div>
