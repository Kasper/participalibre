@extends('layouts.app')

@section('title', 'Confirmation')

@section('content')
<section>
  <h1 class="margin-none">Félicitations, votre demande de contribution a été modifiée !</h1>
  <div class="margin-medium-top columns flex-column flex-middle width-1-2 margin-auto">
    <div>
      <h2 class="text-normal">Que souhaitez-vous faire maintenant ?</h2>
    </div>
    <div>
      <a href="/projects/{{ $project->id }}" class="tile height-medium margin-small">
        <h2>Voir mon annonce</h2>
      </a>
    </div>
    <div>
      <a href="/projects/{{ $project->id }}/edit/{{ $project->admin_token }}" class="tile height-medium margin-small">
        <h2>Modifier mon annonce</h2>
      </a>
    </div>
  </div>
  <div class="margin-medium-top text-center">
    <a class="link" href="{{ route('home') }}">Retour à l’accueil</a>
  </div>
</section>
@endsection
