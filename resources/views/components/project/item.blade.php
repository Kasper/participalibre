<article class="card card-radius">
    <div class="card-header columns flex-wrap">
        <span class="category">{{ $project->tags()->first()->category->name }}</span>
        @foreach ($project->tags as $tag)
        <span class="tag">{{ $tag->name }}</span>
        @endforeach
    </div>
    <hr>
    <a class="text-decoration-none" target="_blank" href="/projects/{{ $project->id }}" style="display: block">
        <div class="card-body">
            <h2 class="text-normal">{{ $project->name }}</h2>
            <p>
                @parsedown(Str::limit($project->description, 255, $end='…'))
            </p>
        </div>
        <div href="/projects/{{ $project->id }}" class="inverse card-footer text-right">
            <i class="arrow-right"></i>
            <span>Voir l’annonce</span>
        </div>
    </a>
</article>